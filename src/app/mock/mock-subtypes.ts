import { CatalogSubType } from '../models/catalogSubType';

import { MOCK_PRICES } from './mock-prices';

export const MOCK_SUBTYPES: CatalogSubType[] = [
  {
    id: 600,
    name: 'PET',
    catalogTypeId: 700,
    description: 'string',
    imageUri: 'string',
    catalogPrices: [MOCK_PRICES[0], MOCK_PRICES[1], MOCK_PRICES[2]]
  },
  {
    id: 601,
    name: 'PP',
    catalogTypeId: 700,
    description: 'string',
    imageUri: 'string',
    catalogPrices: [MOCK_PRICES[3], MOCK_PRICES[4], MOCK_PRICES[5]]
  },
];
