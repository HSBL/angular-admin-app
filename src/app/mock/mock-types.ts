import { CatalogType } from '../models/catalogType';

import { MOCK_SUBTYPES } from './mock-subtypes';

export const MOCK_TYPES: CatalogType[] = [
  {
    id: 700,
    name: 'plastic',
    description: 'string',
    imageUri: 'string',
    catalogSubTypes: [MOCK_SUBTYPES[0], MOCK_SUBTYPES[1]],
  },
  {
    id: 701,
    name: 'paper',
    description: 'string',
    imageUri: 'string',
    catalogSubTypes: [],
  },
];
