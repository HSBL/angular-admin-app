import { CatalogOption } from '../models/catalogOption';

import { MOCK_SUBTYPES } from './mock-subtypes';

export const MOCK_OPTIONS: CatalogOption[] = [
  {
    id: 300,
    name: '350ml',
    imageUri: 'string',
    description: 'string',
    quantifier: 'pcs',
    csr: 20,
    weightInGram: 200,
    catalogSubType: MOCK_SUBTYPES[0]
  },
  {
    id: 301,
    name: '600ml',
    imageUri: 'string',
    description: 'string',
    quantifier: 'pcs',
    csr: 20,
    weightInGram: 200,
    catalogSubType: MOCK_SUBTYPES[1]
  },
  {
    id: 302,
    name: '330ml',
    imageUri: 'string',
    description: 'string',
    quantifier: 'pcs',
    csr: 20,
    weightInGram: 200,
    catalogSubType: MOCK_SUBTYPES[0]
  },
  {
    id: 303,
    name: '1500ml',
    imageUri: 'string',
    description: 'string',
    quantifier: 'pcs',
    csr: 20,
    weightInGram: 200,
    catalogSubType: MOCK_SUBTYPES[1]
  },
];
