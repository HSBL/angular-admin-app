import { CatalogLocation } from '../models/catalogLocation';

export const MOCK_LOCATIONS: CatalogLocation[] = [
  {
    id: 200,
    provinceName: 'Jakarta',
    countryName: 'string',
  },
  {
    id: 201,
    provinceName: 'Jawa Barat',
    countryName: 'string',
  },
  {
    id: 202,
    provinceName: 'Yogyakarta',
    countryName: 'string',
  },
];
