import { CatalogPrice } from '../models/catalogPrice';

import { MOCK_PRICETYPES } from './mock-priceTypes';
import { MOCK_LOCATIONS } from './mock-locations';

export const MOCK_PRICES: CatalogPrice[] = [
  {
    id: 400,
    pricePerUnit: 100,
    priceType: MOCK_PRICETYPES[0]
  },
  {
    id: 401,
    pricePerUnit: 200,
    priceType: MOCK_PRICETYPES[1]
  },
  {
    id: 402,
    pricePerUnit: 300,
    priceType: MOCK_PRICETYPES[2]
  },
  {
    id: 403,
    pricePerUnit: 400,
    priceType: MOCK_PRICETYPES[0]
  },
  {
    id: 404,
    pricePerUnit: 500,
    priceType: MOCK_PRICETYPES[1]
  },
  {
    id: 405,
    pricePerUnit: 600,
    priceType: MOCK_PRICETYPES[2]
  },
  {
    id: 406,
    pricePerUnit: 450,
    priceType: MOCK_PRICETYPES[0],
    location: MOCK_LOCATIONS[0]
  },
  {
    id: 407,
    pricePerUnit: 550,
    priceType: MOCK_PRICETYPES[1],
    location: MOCK_LOCATIONS[0]
  },
  {
    id: 408,
    pricePerUnit: 650,
    priceType: MOCK_PRICETYPES[2],
    location: MOCK_LOCATIONS[0]
  },
];
