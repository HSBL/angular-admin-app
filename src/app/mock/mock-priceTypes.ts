import { CatalogPriceType } from '../models/catalogPriceType';

export const MOCK_PRICETYPES: CatalogPriceType[] = [
  {
    id: 500,
    name: 'tpprice'
  },
  {
    id: 501,
    name: 'mpprice'
  },
  {
    id: 502,
    name: 'mcpprice'
  },
];
