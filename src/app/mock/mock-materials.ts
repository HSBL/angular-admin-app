import { CatalogItem } from '../models/catalogItem';

import { MOCK_OPTIONS } from './mock-options';

export const MOCK_MATERIALS: CatalogItem[] = [
  {
    id: 100,
    name: 'Aqua',
    description: 'mock desription',
    imageUri: 'image uri',
    editorId: 'string',
    catalogOptions: [MOCK_OPTIONS[0], MOCK_OPTIONS[1]]
  },
  {
    id: 101,
    name: 'Ades',
    description: 'mock desription',
    imageUri: 'image uri',
    editorId: 'string',
    catalogOptions: [MOCK_OPTIONS[2], MOCK_OPTIONS[3]]
  },
];
