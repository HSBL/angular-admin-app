
import { CatalogLocation } from './catalogLocation';
import { CatalogPriceType } from './catalogPriceType';

export interface CatalogPrice {
    id?: number;
    pricePerUnit?: number;
    location?: CatalogLocation;
    issuedTime?: Date;
    priceType?: CatalogPriceType;
}
