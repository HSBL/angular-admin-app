import { CatalogOption } from './catalogOption';

export interface CatalogItem {
  id?: number;
  name?: string;
  description?: string;
  imageUri?: string;
  catalogOptions?: Array<CatalogOption>;
  lastUpdated?: Date;
  editorId?: string;
}
