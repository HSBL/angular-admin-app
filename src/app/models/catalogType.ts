import { CatalogSubType } from './catalogSubType';

export interface CatalogType {
    id?: number;
    name?: string;
    description?: string;
    imageUri?: string;
    catalogSubTypes?: Array<CatalogSubType>;
}
