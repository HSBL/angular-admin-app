import { CatalogPrice } from './catalogPrice';

export interface CatalogSubType {
    id?: number;
    name?: string;
    catalogTypeId?: number;
    description?: string;
    imageUri?: string;
    catalogPrices?: Array<CatalogPrice>;
}
