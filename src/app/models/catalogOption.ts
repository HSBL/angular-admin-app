
import { CatalogSubType } from './catalogSubType';

export interface CatalogOption {
    id?: number;
    name?: string;
    imageUri?: string;
    description?: string;
    quantifier?: string;
    csr?: number;
    weightInGram?: number;
    catalogSubType?: CatalogSubType;
    lastUpdated?: Date;
}
