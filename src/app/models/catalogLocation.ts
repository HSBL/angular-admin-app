
export interface CatalogLocation {
    id?: number;
    provinceName?: string;
    countryName?: string;
}
