import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import {
  AddOption,
  RemoveOption,
  AddMaterial,
  RemoveMaterial,
  DestroyOption,
  StorePrice,
  RemovePrice,
  DestroyPrice,
  GetMaterials,
  GetSubTypes,
  GetLocations,
  FetchPrice,
  EditPrice,
  EditPriceOverride,
  DetailMaterial,
  EditMaterial,
  OverrideCsr,
  EditMaterialAddOption,
  EditMaterialEditOption,
  DeleteOption,
  CreateSubType,
  CreateType,
  UploadMaterialPicture,
  UploadOptionPicture,
  EditGlobalPrice
} from '../dashboard/actions/material.actions';
import { CatalogOption } from '../models/catalogOption';
import { CatalogItem } from '../models/catalogItem';
import { CatalogSubType } from '../models/catalogSubType';
import { CatalogPrice } from '../models/catalogPrice';
import { CatalogLocation } from '../models/catalogLocation';

// Services
import { MaterialService } from '../dashboard/material.service';
import { SubtypeService } from '../dashboard/subtype.service';
import { LocationService } from '../dashboard/location.service';
import { PriceService } from '../dashboard/price.service';
import { Router } from '@angular/router';
import { OptionService } from '../dashboard/option.service';

class StateModel {
  locations: CatalogLocation[];
  options: CatalogOption[];
  materials: CatalogItem[];
  subtypes: CatalogSubType[];
  prices: CatalogPrice[];
  material: CatalogItem;
}

@State<StateModel>({
  name: 'state',
  defaults: {
    options: [],
    materials: [],
    subtypes: [],
    locations: [],
    prices: [],
    material: null
  }
})

@Injectable()

export class AppState {

  constructor(
    private materialService: MaterialService,
    private subTypeService: SubtypeService,
    private locationService: LocationService,
    private priceService: PriceService,
    private optionService: OptionService
  ) { }

  @Selector()
  static getMaterialList(state: StateModel) {
    return state.materials;
  }

  @Selector()
  static getSubTypeList(state: StateModel) {
    return state.subtypes;
  }

  @Action(GetMaterials)
  getMaterials({ getState, setState }: StateContext<StateModel>) {
    return this.materialService.fetchMaterials().pipe(tap((result) => {
      const state = getState();
      setState({
        ...state,
        materials: result,
      });
    }));
  }

  @Action(GetLocations)
  getLocations({ getState, setState }: StateContext<StateModel>) {
    return this.locationService.fetcLocations().pipe(tap((result) => {
      const state = getState();
      setState({
        ...state,
        locations: result,
      });
    }));
  }

  @Action(GetSubTypes)
  getSubTypes({ getState, setState }: StateContext<StateModel>) {
    return this.subTypeService.fetcSubTypes().pipe(tap((result) => {
      const state = getState();
      setState({
        ...state,
        subtypes: result,
      });
    }));
  }

  @Action(DetailMaterial)
  detailMaterial({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: DetailMaterial) {
    return this.materialService.fetchDetail(payload).pipe(tap((result) => {
      dispatch(new DestroyOption());
      result.catalogOptions.forEach(option => {
        dispatch(new AddOption(option));
      });
      const state = getState();
      patchState({
        material: { ...state, ...result }
      });
    }));
  }

  @Action(EditMaterial)
  editMaterial({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: EditMaterial) {
    return this.materialService.editMaterial(payload).pipe(tap((result) => {
      return dispatch(new GetMaterials());
    }));
  }

  @Action(OverrideCsr)
  overrideCsr({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: OverrideCsr) {
    return this.materialService.overrideCsr(payload).pipe(tap((result) => {
      return dispatch(new GetMaterials());
    }));
  }

  @Action(FetchPrice)
  fetchPrice({ getState, setState, dispatch }: StateContext<StateModel>, { payload }: FetchPrice) {
    return this.subTypeService.fetchPriceByLocation(payload).pipe(tap((result) => {
      return result.forEach(price => {
        dispatch(new StorePrice(price));
      });
    }));
  }

  @Action(EditPrice)
  editPrice({ getState, setState }: StateContext<StateModel>, { payload }: EditPrice) {
    return this.priceService.editPrice(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(EditGlobalPrice)
  editGlobalPrice({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: EditGlobalPrice) {
    return this.priceService.editGlobalPrice(payload).pipe(tap((result) => {
      dispatch(new GetSubTypes());
      // update prices
      const state = getState();
      const prices = state.prices;
      prices.forEach(element => {
        if (element.priceType.id === payload.catalogPrices[0].priceType.id) {
          element.pricePerUnit = payload.catalogPrices[0].pricePerUnit;
        }
      });
      patchState({
        prices: [...state.prices.filter(a => false), ...prices]
      });
      return result;
    }));
  }

  @Action(EditPriceOverride)
  editPriceOverride({ getState, setState }: StateContext<StateModel>, { payload }: EditPriceOverride) {
    return this.priceService.editPriceOverride(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(AddMaterial)
  addMaterial({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: AddMaterial) {
    return this.materialService.createMaterial(payload).pipe(tap((result) => {
      return dispatch(new GetMaterials());
    }));
  }

  @Action(UploadMaterialPicture)
  uploadMaterialPicture({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: UploadMaterialPicture) {
    const state = getState();
    return this.materialService.uploadPicture(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(AddOption)
  add({ getState, patchState }: StateContext<StateModel>, { payload }: AddOption) {
    const state = getState();
    patchState({
      options: [...state.options, payload]
    });
  }

  @Action(EditMaterialAddOption)
  editMaterialAddOption({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: EditMaterialAddOption) {
    const state = getState();
    return this.optionService.addOption(payload).pipe(tap((result) => {
      return dispatch(new DetailMaterial(payload.itemId));
    }));
  }

  @Action(EditMaterialEditOption)
  editMaterialEditOption({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: EditMaterialEditOption) {
    const state = getState();
    return this.optionService.editOption(payload).pipe(tap((result) => {
      return dispatch(new DetailMaterial(payload.itemId));
    }));
  }

  @Action(DeleteOption)
  deleteOption({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: DeleteOption) {
    const state = getState();
    return this.optionService.deleteOption(payload).pipe(tap((result) => {
      return result;
      // return dispatch(new DetailMaterial(payload.itemId)); // local state handled by removing option
    }));
  }

  @Action(UploadOptionPicture)
  uploadOptionPicture({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: UploadOptionPicture) {
    const state = getState();
    return this.optionService.uploadPicture(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(RemoveOption)
  remove({ getState, patchState }: StateContext<StateModel>, { payload }: RemoveOption) {
    patchState({
      options: getState().options.filter(a => a.name !== payload)
    });
  }

  @Action(DestroyOption)
  destroy({ getState, patchState }: StateContext<StateModel>) {
    patchState({
      options: getState().options.filter(a => false)
    });
  }

  @Action(RemoveMaterial)
  removeMaterial({ getState, patchState }: StateContext<StateModel>, { payload }: RemoveMaterial) {
    return this.materialService.deleteMaterial(payload).pipe(tap((result) => {
      patchState({
        materials: getState().materials.filter(a => a.id !== payload)
      });
    }));
  }

  @Action(StorePrice)
  storePrice({ getState, patchState }: StateContext<StateModel>, { payload }: StorePrice) {
    const state = getState();
    patchState({
      prices: [...state.prices, payload]
    });
  }

  @Action(RemovePrice)
  removePrice({ getState, patchState }: StateContext<StateModel>, { payload }: RemovePrice) {
    patchState({
      prices: getState().prices.filter(a => a.id !== payload)
    });
  }

  @Action(DestroyPrice)
  destroyPrice({ getState, patchState }: StateContext<StateModel>) {
    patchState({
      prices: getState().prices.filter(a => false)
    });
  }

  // SEED PURPOSE
  @Action(CreateSubType)
  createSubType({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: CreateSubType) {
    return this.subTypeService.createSubType(payload).pipe(tap((result) => {
      return dispatch(new GetSubTypes());
    }));
  }

  @Action(CreateType)
  createType({ getState, patchState, dispatch }: StateContext<StateModel>, { payload }: CreateType) {
    return this.subTypeService.createType(payload).pipe(tap((result) => {
      return dispatch(new GetSubTypes());
    }));
  }
}
