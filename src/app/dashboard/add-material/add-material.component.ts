import { Component, OnInit } from '@angular/core';
import { Router, ChildActivationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CatalogOption } from '../../models/catalogOption';
import { CatalogItem } from '../../models/catalogItem';
import { RemoveOption, AddMaterial, DestroyOption, GetSubTypes, UploadMaterialPicture } from '../actions/material.actions';

@Component({
  selector: 'app-add-material',
  templateUrl: './add-material.component.html',
  styleUrls: ['./add-material.component.scss']
})
export class AddMaterialComponent implements OnInit {

  options$: Observable<CatalogOption[]>;

  materialForm = this.fb.group({
    name: [''],
    description: [''],
    editorId: ['editor'],
    imageUri: ['image.jpg'],
    image: [''],
    catalogOptions: [null]
  });


  constructor(
    private router: Router,
    private location: Location,
    private store: Store,
    private fb: FormBuilder
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.materialForm.patchValue(this.router.getCurrentNavigation().extras.state.cachedMaterial);
    } else {
      console.log('NEW FORM');
    }
    this.options$ = this.store.select(response => response.state.options);
  }

  ngOnInit(): void {
    this.store.dispatch(new GetSubTypes());
  }

  cancel(): void {
    this.store.dispatch(new DestroyOption());
    this.router.navigate(['dashboard', 'global-material']);
  }
  save(): void {
    if (!this.materialForm.valid) {
      return console.log('FORM NOT VALID');
    }
    let optionsLength: number;
    this.options$.subscribe(val => {
      this.materialForm.patchValue({ catalogOptions: val });
      return optionsLength = val.length;
    });
    if (optionsLength < 1) {
      return alert('must input option');
    }
    if (this.materialForm.value.image === '') {
      return alert('Select Image');
    }
    this.store.dispatch(new UploadMaterialPicture(
      {
        image: this.materialForm.value.image.split(',')[1],
        name: this.materialForm.value.imageUri
      }));
    this.store.dispatch(new AddMaterial(this.materialForm.value));
    this.store.dispatch(new DestroyOption());
    this.router.navigate(['dashboard', 'global-material']);
  }
  addOption(): void {
    this.router.navigate(['dashboard', 'add-option'], { state: { materialForm: this.materialForm.value } });
  }
  deleteOption(option): void {
    this.store.dispatch(new RemoveOption(option.name));
  }
  editOption(option: CatalogOption): void {
    console.log(option);
    this.router.navigate(['dashboard', 'edit-option'], { state: { option, materialForm: this.materialForm.value } });
  }
  // TODO: file input validation
  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.materialForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.materialForm.patchValue({ image: Reader.result });
    };
    Reader.readAsDataURL(file);
  }
  getTpPrice(option: CatalogOption): number {
    return option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'tpprice')[0].pricePerUnit
      + (option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'tpprice')[0].pricePerUnit * option.csr / 100);
  }
  getMpPrice(option: CatalogOption): number {
    return option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mpprice')[0].pricePerUnit
      + (option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mpprice')[0].pricePerUnit * option.csr / 100);
  }
  getMcpPrice(option: CatalogOption): number {
    return option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mcpprice')[0].pricePerUnit
      + (option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mcpprice')[0].pricePerUnit * option.csr / 100);
  }
}
