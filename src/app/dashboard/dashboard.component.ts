import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  vw = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
  sidenavOpen: boolean;
  mobileHelper: boolean;

  constructor() { }

  ngOnInit(): void {
    if (this.vw < 800) {
      this.sidenavOpen = false;
      this.mobileHelper = true;
    } else {
      this.sidenavOpen = true;
    }
  }
}
