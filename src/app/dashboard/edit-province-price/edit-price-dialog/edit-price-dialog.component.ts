import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';

import { Store } from '@ngxs/store';
import { StorePrice, RemovePrice, EditPrice, EditPriceOverride } from '../../actions/material.actions';
import { CatalogPrice } from 'src/app/models/catalogPrice';

@Component({
  selector: 'app-edit-price-dialog',
  templateUrl: './edit-price-dialog.component.html',
  styleUrls: ['./edit-price-dialog.component.scss']
})
export class EditPriceDialogComponent implements OnInit {

  overwrite = false;
  newCatalogPrice: CatalogPrice;
  newPriceForm = this.fb.group({
    newPrice: [null, Validators.min(0)],
    overwrite: [null]
  });

  constructor(
    public dialogRef: MatDialogRef<EditPriceDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private store: Store,
  ) { }

  ngOnInit(): void {
  }
  cancel(): void {
    this.dialogRef.close();
  }
  apply(): void {
    if (!this.newPriceForm.valid) {
      return console.log('FORM NOT VALID');
    }
    if (this.newPriceForm.value.overwrite) {
      // return console.log({
      //   id: this.data.id,
      //   pricePerUnit: this.newPriceForm.value.newPrice,
      //   catalogSubTypeId: this.data.subTypeId,
      //   editorId: 'override'
      // });
      this.store.dispatch(new EditPriceOverride(
        {
          id: this.data.id,
          pricePerUnit: this.newPriceForm.value.newPrice,
          catalogSubTypeId: this.data.subTypeId,
          editorId: 'override'
        }));
    } else {
      this.store.dispatch(new EditPrice({ id: this.data.id, pricePerUnit: this.newPriceForm.value.newPrice }));
    }
    this.store.dispatch(new RemovePrice(this.data.id));
    this.store.dispatch(new StorePrice({ ...this.data, pricePerUnit: this.newPriceForm.value.newPrice }));
    this.dialogRef.close();
  }
}
