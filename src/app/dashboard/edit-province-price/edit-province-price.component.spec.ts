import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditProvincePriceComponent } from './edit-province-price.component';

describe('EditProvincePriceComponent', () => {
  let component: EditProvincePriceComponent;
  let fixture: ComponentFixture<EditProvincePriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditProvincePriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditProvincePriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
