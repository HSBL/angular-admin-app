import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { CatalogSubType } from '../../models/catalogSubType';
import { CatalogPrice } from '../../models/catalogPrice';
import { EditPriceDialogComponent } from './edit-price-dialog/edit-price-dialog.component';

import { FetchPrice, DestroyPrice } from '../actions/material.actions';
import { CatalogLocation } from 'src/app/models/catalogLocation';

@Component({
  selector: 'app-edit-province-price',
  templateUrl: './edit-province-price.component.html',
  styleUrls: ['./edit-province-price.component.scss']
})
export class EditProvincePriceComponent implements OnInit {

  prices$: Observable<CatalogPrice[]>;
  subtypes$: Observable<CatalogSubType[]>;
  selectedSubtype: CatalogSubType;
  catalogLocation: CatalogLocation;

  constructor(
    private router: Router,
    private location: Location,
    private store: Store,
    public dialog: MatDialog
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      // Clear the store first
      this.store.dispatch(new DestroyPrice());
      this.selectedSubtype = this.router.getCurrentNavigation().extras.state.subtype;
      this.catalogLocation = this.router.getCurrentNavigation().extras.state.location;

      // Store Initial price to state management
      this.store.dispatch(new FetchPrice({ subTypeId: this.selectedSubtype.id, locationId: this.catalogLocation.id }));
      // this.router.getCurrentNavigation().extras.state.subtype.catalogPrices.forEach(price => {
      //   this.store.dispatch(new StorePrice(price));
      // });
    } else {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
    this.prices$ = this.store.select(response => response.state.prices);

    this.subtypes$ = this.store.select(response => response.state.subtypes);
  }

  ngOnInit(): void {
  }

  // methods
  onChange(payload) {
    console.log(payload);
  }
  // cancel(): void {
  //   this.location.back();
  //   // this.store.dispatch(new DestroyPrice());
  // }
  save(): void {
    // Send Stored Data to Backend
    this.router.navigate(['dashboard']);
  }
  editPriceDialog(price): void {
    price.subTypeId = this.selectedSubtype.id;
    this.dialog.open(EditPriceDialogComponent, { data: price });
  }
}
