import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { CatalogSubType } from '../../models/catalogSubType';
import { DestroyOption, DestroyPrice, GetSubTypes, GetLocations, CreateSubType, CreateType } from '../actions/material.actions';
import { CatalogLocation } from 'src/app/models/catalogLocation';
import { LoginComponent } from 'src/app/authentication/login/login.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  subtypes$: Observable<CatalogSubType[]>;
  locations$: Observable<CatalogLocation[]>;

  editProvinceForm = this.fb.group({
    location: [null],
    subtype: [null],
  });

  editGlobalForm = this.fb.group({
    subtype: [null],
  });

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) {
    this.subtypes$ = this.store.select(response => response.state.subtypes);
    this.locations$ = this.store.select(response => response.state.locations);
  }

  ngOnInit(): void {
    this.store.dispatch(new DestroyOption());
    this.store.dispatch(new DestroyPrice());
    this.store.dispatch(new GetSubTypes());
    this.store.dispatch(new GetLocations());
  }

  editProvince(): void {
    if (!this.editProvinceForm.valid) {
      return console.log('FORM NOT VALID');
    }
    // TODO: Routing based fetch data?
    this.router.navigate(['dashboard', 'edit-province-price'], { state: this.editProvinceForm.value });
  }

  editGlobalPrice(): void {
    if (!this.editGlobalForm.valid) {
      return console.log('FORM NOT VALID');
    }
    this.router.navigate(['dashboard', 'edit-global-price'], { state: this.editGlobalForm.value });
  }
}
