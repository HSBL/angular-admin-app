import { Component, OnInit } from '@angular/core';
import { Router, RoutesRecognized } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from '@angular/forms';

import { filter, pairwise } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddOption, UploadOptionPicture } from '../actions/material.actions';

// models
import { CatalogSubType } from '../../models/catalogSubType';
import { CatalogItem } from '../../models/catalogItem';
import { CatalogPrice } from 'src/app/models/catalogPrice';
import { CatalogOption } from 'src/app/models/catalogOption';

@Component({
  selector: 'app-add-option',
  templateUrl: './add-option.component.html',
  styleUrls: ['./add-option.component.scss']
})
export class AddOptionComponent implements OnInit {

  previousUrl: string;
  cachedMaterial: CatalogItem;
  options: CatalogOption[];
  unique = true;
  subtypes$: Observable<CatalogSubType[]>;
  selectedSubtype: CatalogSubType;
  quantifiers: string[] = ['pcs', 'kilo', 'ons', 'unit', 'pack'];

  optionForm = this.fb.group({
    name: [''],
    weightInGram: [null, Validators.min(1)],
    csr: [0, Validators.min(0)],
    quantifier: [''],
    catalogSubTypeId: [null],
    imageUri: [''],
    description: [''],
  });

  constructor(
    private router: Router,
    private location: Location,
    private fb: FormBuilder,
    private store: Store
  ) {
    if (!this.router.getCurrentNavigation().extras.state) {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
    this.cachedMaterial = this.router.getCurrentNavigation().extras.state.materialForm;
    this.subtypes$ = this.store.select(response => response.state.subtypes);
    this.options = this.store.selectSnapshot(response => response.state.options);
  }

  ngOnInit(): void {
  }

  cancel(): void {
    // TODO: delete image stored on bucket
    this.router.navigate(['dashboard', 'add-material'], { state: { cachedMaterial: this.cachedMaterial } });
  }
  save(): void {
    if (!this.optionForm.valid) {
      return console.log('FORM NOT VALID');
    }
    // Validate other option stored at state
    this.unique = true;
    this.options.forEach(option => {
      if (option.name === this.optionForm.value.name) {
        this.unique = false;
      }
    });
    if (!this.unique) {
      return alert('OPTION NAME ALREADY USED');
    }
    if (this.optionForm.value.imageUri === '') {
      return alert('Select Option Image');
    }
    this.optionForm.patchValue({ catalogSubTypeId: this.selectedSubtype.id });
    this.store.dispatch(new AddOption(this.optionForm.value));
    this.router.navigate(['dashboard', 'add-material'], { state: { cachedMaterial: this.cachedMaterial } });
  }
  getSubsidied(value): number {
    return value + value * this.optionForm.value.csr / 100;
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.optionForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadOptionPicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.optionForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }
}
