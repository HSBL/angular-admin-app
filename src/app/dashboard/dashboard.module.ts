import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';

import { MaterialListComponent } from './material-list/material-list.component';
import { HomeComponent } from './home/home.component';
import { DashboardComponent } from './dashboard.component';
import { AddMaterialComponent } from './add-material/add-material.component';
import { AddOptionComponent } from './add-option/add-option.component';
import { EditOptionComponent } from './edit-option/edit-option.component';
import { EditGlobalPriceComponent } from './edit-global-price/edit-global-price.component';
import { EditProvincePriceComponent } from './edit-province-price/edit-province-price.component';
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule } from '@angular/forms';


import { NgxsModule } from '@ngxs/store';
import { AppState } from '../state/state';
import { EditPriceDialogComponent } from './edit-province-price/edit-price-dialog/edit-price-dialog.component';
import { DeleteDialogComponent } from './material-list/delete-dialog/delete-dialog.component';
import { ManagePartnerAccountsComponent } from './other-pages/manage-partner-accounts/manage-partner-accounts.component';
import { AddUserAccountComponent } from './other-pages/add-user-account/add-user-account.component';
import { ManageInternalAccountComponent } from './other-pages/manage-internal-account/manage-internal-account.component';
import { HttpClientModule } from '@angular/common/http';
import { EditGlobalDialogComponent } from './edit-global-price/edit-global-dialog/edit-global-dialog.component';

@NgModule({
  declarations: [
    MaterialListComponent,
    HomeComponent,
    DashboardComponent,
    AddMaterialComponent,
    AddOptionComponent,
    EditOptionComponent,
    EditGlobalPriceComponent,
    EditProvincePriceComponent,
    EditPriceDialogComponent,
    DeleteDialogComponent,
    ManagePartnerAccountsComponent,
    AddUserAccountComponent,
    ManageInternalAccountComponent,
    EditGlobalDialogComponent
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgxsModule.forFeature([
      AppState
    ])
  ]
})
export class DashboardModule { }
