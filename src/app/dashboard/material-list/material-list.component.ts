import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';

import { Store } from '@ngxs/store';
import { Observable, Subscription } from 'rxjs';
import { RemoveMaterial, DestroyOption, DestroyPrice } from '../actions/material.actions';
import { CatalogItem } from '../../models/catalogItem';
import { MatDialog } from '@angular/material/dialog';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';

import { GetMaterials } from '../actions/material.actions';

@Component({
  selector: 'app-material-list',
  templateUrl: './material-list.component.html',
  styleUrls: ['./material-list.component.scss']
})
export class MaterialListComponent implements OnInit, OnDestroy {

  materials$: Observable<CatalogItem[]>;
  private observableSub: Subscription;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  displayedColumns: string[] = ['item', 'editor', 'updated', 'action'];
  dataSource: any;

  constructor(
    private store: Store,
    private router: Router,
    public dialog: MatDialog
  ) {
    this.materials$ = this.store.select(response => response.state.materials);
  }

  ngOnInit(): void {
    this.observableSub = this
      .materials$
      .subscribe(vals => {
        this.dataSource = new MatTableDataSource(vals);
        this.dataSource.paginator = this.paginator;
      });
    this.store.dispatch(new DestroyOption());
    this.store.dispatch(new DestroyPrice());
    this.store.dispatch(new GetMaterials());
  }

  ngOnDestroy() {
    this.observableSub.unsubscribe();
  }

  confirmDelete(id: number): void {
    const dialogRef = this.dialog.open(DeleteDialogComponent);
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'delete') {
        this.store.dispatch(new RemoveMaterial(id));
      }
    });
  }
  editMaterial(material: CatalogItem): void {
    this.router.navigate(['dashboard', 'edit-material', material.id]);
  }

  search(filter: string) {
    this.dataSource.filter = filter.trim().toLowerCase();
  }
}
