import { CatalogOption } from '../../models/catalogOption';
import { CatalogItem } from '../../models/catalogItem';
import { CatalogPrice } from '../../models/catalogPrice';
import { CatalogSubType } from 'src/app/models/catalogSubType';

export class AddOption {
  static readonly type = '[OPTION] add';

  constructor(public payload: CatalogOption) { }
}

export class EditMaterialAddOption {
  static readonly type = '[OPTION] editMaterialAddoption';

  constructor(public payload: any) { }
}

export class EditMaterialEditOption {
  static readonly type = '[OPTION] editMaterialEditoption';

  constructor(public payload: any) { }
}

export class UploadOptionPicture {
  static readonly type = '[OPTION] uploadOptionPicture';

  constructor(public payload: any) { }
}

export class DeleteOption {
  static readonly type = '[OPTION] deleteOption';

  constructor(public payload: any) { }
}

export class RemoveOption {
  static readonly type = '[OPTION] remove';

  constructor(public payload: string) { }
}

export class DestroyOption {
  static readonly type = '[OPTION] destroy';

  constructor() { }
}

export class GetMaterials {
  static readonly type = '[MATERIAL] Get';
}

export class GetSubTypes {
  static readonly type = '[SUBTYPE] GetSubTypes';
}

export class GetLocations {
  static readonly type = '[LOCATION] GetLocations';
}

export class AddMaterial {
  static readonly type = '[MATERIAL] addMaterial';

  constructor(public payload: CatalogItem) { }
}

export class UploadMaterialPicture {
  static readonly type = '[MATERIAL] uploadMaterialPicture';

  constructor(public payload: any) { }
}

export class DetailMaterial {
  static readonly type = '[MATERIAL] detailMaterial';

  constructor(public payload: number) { }
}

export class EditMaterial {
  static readonly type = '[MATERIAL] editMaterial';

  constructor(public payload: CatalogItem) { }
}

export class OverrideCsr {
  static readonly type = '[MATERIAL] overrideCsr';

  constructor(public payload: any) { }
}

export class RemoveMaterial {
  static readonly type = '[MATERIAL] removeMaterial';

  constructor(public payload: number) { }
}

export class FetchPrice {
  static readonly type = '[PRICE] fetchPrice';

  constructor(public payload: any) { }
}

export class EditPrice {
  static readonly type = '[PRICE] editPrice';

  constructor(public payload: CatalogPrice) { }
}

export class EditGlobalPrice {
  static readonly type = '[PRICE] editGlobalPrice';

  constructor(public payload: CatalogSubType) { }
}

export class EditPriceOverride {
  static readonly type = '[PRICE] editPriceOverride';

  constructor(public payload: any) { }
}

export class StorePrice {
  static readonly type = '[PRICE] storePrice';

  constructor(public payload: CatalogPrice) { }
}

export class RemovePrice {
  static readonly type = '[PRICE] removePrice';

  constructor(public payload: number) { }
}
export class DestroyPrice {
  static readonly type = '[PRICE] destroyPrice';

  constructor() { }
}

// SEED PURPOSE
export class CreateSubType {
  static readonly type = '[SUBTYPE] createSubType';

  constructor(public payload: any) { }
}
export class CreateType {
  static readonly type = '[SUBTYPE] createType';

  constructor(public payload: any) { }
}
