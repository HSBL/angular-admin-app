import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { CatalogOption } from '../models/catalogOption';
import { CatalogSubType } from '../models/catalogSubType';

@Injectable({
  providedIn: 'root'
})
export class OptionService {

  constructor(private http: HttpClient) { }

  addOption(payload: any) {
    return this.http.post<any>(environment.apiUrl + `/api/v1/CatalogItem/${payload.itemId}/AddOption`, payload.option);
  }

  editOption(payload: any) {
    return this.http.put<any>(
      environment.apiUrl + `/api/v1/CatalogItem/${payload.itemId}/EditOption/${payload.option.id}`, payload.option);
  }

  uploadPicture(payload: any) {
    return this.http.post<any>(environment.apiUrl + '/api/v1/CatalogOption/UploadPicture', payload);
  }

  deleteOption(payload: any) {
    return this.http.delete(environment.apiUrl + `/api/v1/catalogoption/${payload.optionId}`);
  }
}
