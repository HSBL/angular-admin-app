import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { CatalogSubType } from '../models/catalogSubType';
import { CatalogPrice } from '../models/catalogPrice';

@Injectable({
  providedIn: 'root'
})
export class SubtypeService {

  constructor(private http: HttpClient) { }

  fetcSubTypes() {
    return this.http.get<CatalogSubType[]>(environment.apiUrl + '/api/v1/CatalogSubType/all');
  }

  fetchPriceByLocation(payload: any) {
    return this.http.get<CatalogPrice[]>(
      environment.apiUrl + `/api/v1/CatalogSubType/${payload.subTypeId}/pricebylocation/${payload.locationId}`);
  }

  // SEED PURPOSE
  createSubType(payload: any) {
    return this.http.post<any>(environment.apiUrl + `/api/v1/CatalogType/${payload.catalogTypeId}/AddSubType`, payload);
  }

  createType(payload: any) {
    return this.http.post<any>(environment.apiUrl + `/api/v1/CatalogType/save`, payload);
  }
}
