import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from '../../environments/environment';

import { CatalogItem } from '../models/catalogItem';
import { HttpClient } from '@angular/common/http';
import { MOCK_MATERIALS } from '../mock/mock-materials';

@Injectable({
  providedIn: 'root'
})
export class MaterialService {

  materials: CatalogItem[] = MOCK_MATERIALS;

  constructor(private http: HttpClient) { }

  fetchMaterials() {
    return this.http.get<CatalogItem[]>(environment.apiUrl + '/api/v1/CatalogItem/global');
  }

  fetchDetail(id: number) {
    return this.http.get<CatalogItem>(environment.apiUrl + '/api/v1/CatalogItem/' + id);
  }

  createMaterial(payload: CatalogItem) {
    return this.http.post<CatalogItem>(environment.apiUrl + '/api/v1/catalogitem/save', payload);
  }

  editMaterial(payload: CatalogItem) {
    return this.http.put<CatalogItem>(environment.apiUrl + '/api/v1/catalogitem/' + payload.id, payload);
  }

  overrideCsr(payload: any) {
    return this.http.put<any>(environment.apiUrl + '/api/v1/catalogitem/' + payload.id + '/EditItemCsr', payload);
  }

  uploadPicture(payload: any) {
    return this.http.post<any>(environment.apiUrl + '/api/v1/catalogitem/UploadPicture', payload);
  }

  deleteMaterial(id: number) {
    return this.http.delete(environment.apiUrl + `/api/v1/CatalogItem/${id}`);
  }
}
