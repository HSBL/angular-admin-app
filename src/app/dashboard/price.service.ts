import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { CatalogPrice } from '../models/catalogPrice';
import { CatalogSubType } from '../models/catalogSubType';

@Injectable({
  providedIn: 'root'
})
export class PriceService {

  constructor(private http: HttpClient) { }

  fetchPrice() {
    return this.http.get<CatalogPrice[]>(environment.apiUrl + '/api/v1/CatalogPrice/all');
  }

  editPrice(payload: CatalogPrice) {
    return this.http.put<CatalogPrice>(environment.apiUrl + `/api/v1/CatalogPrice/${payload.id}`, payload);
  }

  editGlobalPrice(payload: CatalogSubType) {
    return this.http.put<CatalogSubType>(
      environment.apiUrl + `/api/v1/CatalogType/${
      payload.catalogTypeId
      }/EditGlobalPrice?subTypeId=${
      payload.id
      }&priceTypeId=${payload.catalogPrices[0].priceType.id}`, { pricePerUnit: payload.catalogPrices[0].pricePerUnit });
  }

  editPriceOverride(payload: any) {
    return this.http.put<any>(environment.apiUrl + `/api/v1/CatalogPrice/${payload.id}/overridecsr`, payload);
  }
}
