import { State, Action, StateContext, Selector } from '@ngxs/store';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import {
  FetchType,
  PreserveType,
  CreateType,
  UploadTypePicture,
  RemoveType,
  AddSubType,
  AddSubTypeToEditedType,
  UploadSubTypePicture,
  RemoveSubType,
  DeleteSubType,
  CleanSubType,
  UpdateType,
  EditSubTypeToEditedType
} from '../actions/type.actions';

// MODELS
import { CatalogSubType } from 'src/app/models/catalogSubType';
import { CatalogType } from 'src/app/models/catalogType';

// SERVICES
import { TypeService } from '../type.service';
import { SubtypeService } from '../subtype.service';

export class TypeStateModel {
  type: CatalogType;
  types: CatalogType[];
  subtypes: CatalogSubType[];
}

@State<TypeStateModel>({
  name: 'type',
  defaults: {
    type: { name: '', description: '', imageUri: '' },
    types: [],
    subtypes: [],
  }
})

@Injectable()

export class TypeState {

  constructor(
    private typeService: TypeService,
    private subTypeService: SubtypeService
  ) { }

  @Selector()
  static getLocalSubTypes(state: TypeStateModel) {
    return state.subtypes;
  }

  @Selector()
  static getTypes(state: TypeStateModel) {
    return state.types;
  }

  @Selector()
  static getPreservedType(state: TypeStateModel) {
    return state.type;
  }

  @Action(FetchType)
  fetchType({ getState, setState }: StateContext<TypeStateModel>) {
    return this.typeService.fetchType().pipe(tap((result) => {
      const state = getState();
      setState({
        ...state,
        types: result
      });
    }));
  }

  @Action(PreserveType)
  preserveType({ getState, patchState }: StateContext<TypeStateModel>, { payload }: PreserveType) {
    const state = getState();
    patchState({
      type: { ...state.type, ...payload }
    });
  }

  @Action(CreateType)
  createType({ getState, patchState, dispatch }: StateContext<TypeStateModel>, { payload }: CreateType) {
    return this.typeService.createType(payload).pipe(tap((result) => {
      return dispatch(new FetchType());
    }));
  }

  @Action(UpdateType)
  updateType({ getState, patchState, dispatch }: StateContext<TypeStateModel>, { payload }: UpdateType) {
    return this.typeService.updateType(payload).pipe(tap((result) => {
      return dispatch(new FetchType());
    }));
  }

  @Action(UploadTypePicture)
  UploadTypePicture({ getState, patchState, dispatch }: StateContext<TypeStateModel>, { payload }: UploadTypePicture) {
    return this.typeService.uploadPicture(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(AddSubType)
  addSubType({ getState, patchState }: StateContext<TypeStateModel>, { payload }: AddSubType) {
    const state = getState();
    patchState({
      subtypes: [...state.subtypes, payload]
    });
  }

  @Action(AddSubTypeToEditedType)
  addSubTypeToEditedType({ getState, patchState }: StateContext<TypeStateModel>, { payload }: AddSubTypeToEditedType) {
    const state = getState();
    return this.subTypeService.addSubType(payload).pipe(tap((result) => {
      return patchState({
        subtypes: [...state.subtypes, payload]
      });
    }));
  }

  @Action(EditSubTypeToEditedType)
  editSubTypeToEditedType({ getState, patchState }: StateContext<TypeStateModel>, { payload }: EditSubTypeToEditedType) {
    const state = getState();
    return this.subTypeService.UpdateSubType(payload).pipe(tap((result) => {
      // return patchState({
      //   subtypes: [...state.subtypes, payload]
      // });
      return result;
    }));
  }

  @Action(UploadSubTypePicture)
  uploadSubTypePicture({ getState, patchState, dispatch }: StateContext<TypeStateModel>, { payload }: UploadSubTypePicture) {
    return this.subTypeService.uploadPicture(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(RemoveSubType)
  removeSubType({ getState, patchState }: StateContext<TypeStateModel>, { payload }: RemoveSubType) {
    const state = getState();
    patchState({
      subtypes: getState().subtypes.filter(a => a.name !== payload)
    });
  }

  @Action(DeleteSubType)
  deleteSubType({ getState, patchState }: StateContext<TypeStateModel>, { payload }: DeleteSubType) {
    return this.subTypeService.deleteSubType(payload).pipe(tap((result) => {
      return result;
    }));
  }

  @Action(CleanSubType)
  cleanSubType({ getState, patchState }: StateContext<TypeStateModel>) {
    const state = getState();
    patchState({
      subtypes: getState().subtypes.filter(a => false)
    });
  }
}
