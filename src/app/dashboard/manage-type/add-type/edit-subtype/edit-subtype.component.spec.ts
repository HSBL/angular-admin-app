import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubtypeComponent } from './edit-subtype.component';

describe('EditSubtypeComponent', () => {
  let component: EditSubtypeComponent;
  let fixture: ComponentFixture<EditSubtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSubtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
