import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormArray, FormControl } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddSubType, UploadSubTypePicture, RemoveSubType } from '../../actions/type.actions';

@Component({
  selector: 'app-edit-subtype',
  templateUrl: './edit-subtype.component.html',
  styleUrls: ['./edit-subtype.component.scss']
})
export class EditSubtypeComponent implements OnInit {

  previousName: string;
  editSubTypeForm = this.fb.group(
    {
      name: [''],
      description: [''],
      imageUri: [''],
      catalogPrices: [null],
      tpprice: [''],
      mpprice: [''],
      mcpprice: [''],
    }
  );

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.editSubTypeForm.patchValue(this.router.getCurrentNavigation().extras.state.subType);
      this.previousName = this.router.getCurrentNavigation().extras.state.subType.name;
    } else {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit(): void {
  }

  save(): void {
    if (!this.editSubTypeForm.valid) {
      return console.log('FORM NOT VALID');
    }
    this.editSubTypeForm.patchValue({
      catalogPrices: [
        {
          pricePerUnit: this.editSubTypeForm.value.tpprice,
          priceTypeId: 1
        },
        {
          pricePerUnit: this.editSubTypeForm.value.mpprice,
          priceTypeId: 2
        },
        {
          pricePerUnit: this.editSubTypeForm.value.mcpprice,
          priceTypeId: 3
        }
      ]
    });
    if (this.editSubTypeForm.value.imageUri === '') {
      return alert('Select Image');
    }
    // validate uniq
    this.store.dispatch(new RemoveSubType(this.previousName));
    this.store.dispatch(new AddSubType(this.editSubTypeForm.value));
    this.router.navigate(['dashboard', 'manage-type', 'add-type']);
  }

  cancel(): void {
    this.router.navigate(['dashboard', 'manage-type', 'add-type']);
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.editSubTypeForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadSubTypePicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.editSubTypeForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }
}
