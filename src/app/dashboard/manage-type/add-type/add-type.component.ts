import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';

import { Store } from '@ngxs/store';
import { Select } from '@ngxs/store';
import { TypeState } from '../state/type.state';
import {
  RemoveSubType,
  CleanSubType,
  UploadTypePicture,
  CreateType,
  PreserveType,
  CleanType
} from '../actions/type.actions';

import { CatalogSubType } from 'src/app/models/catalogSubType';
import { CatalogType } from 'src/app/models/catalogType';

@Component({
  selector: 'app-add-type',
  templateUrl: './add-type.component.html',
  styleUrls: ['./add-type.component.scss']
})
export class AddTypeComponent implements OnInit {

  @Select(TypeState.getLocalSubTypes) subTypes$: Observable<CatalogSubType[]>;
  @Select(TypeState.getPreservedType) preservedType$: Observable<CatalogType>;

  addTypeForm = this.fb.group(
    {
      name: [''],
      description: [''],
      imageUri: [''],
      image: [null],
      catalogSubTypes: [null]
    }
  );

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store
  ) {
    this.preservedType$.subscribe(val => {

      return this.addTypeForm.patchValue({
        name: val.name,
        description: val.description,
        imageUri: val.imageUri
      });
    });
  }

  ngOnInit(): void {
  }

  save(): void {
    if (!this.addTypeForm.valid) {
      return console.log('FORM NOT VALID');
    }
    let subTypesLength: number;
    this.subTypes$.subscribe(val => {
      this.addTypeForm.patchValue({ catalogSubTypes: val });
      return subTypesLength = val.length;
    });
    if (subTypesLength < 1) {
      return alert('must input subtype');
    }
    if (this.addTypeForm.value.imageUri === '') {
      return alert('Select Image');
    }
    this.store.dispatch(new UploadTypePicture(
      {
        image: this.addTypeForm.value.image.split(',')[1],
        name: this.addTypeForm.value.imageUri
      }));
    this.store.dispatch(new CreateType(this.addTypeForm.value));
    this.store.dispatch(new CleanSubType());
    this.store.dispatch(new CleanType());
    this.router.navigate(['dashboard', 'manage-type']);
  }

  cancel(): void {
    this.store.dispatch(new CleanType());
    this.store.dispatch(new CleanSubType());
    this.router.navigate(['dashboard', 'manage-type']);
  }

  addSubtype(): void {
    this.store.dispatch(new PreserveType(this.addTypeForm.value));
    this.router.navigate(['add-subtype'], { relativeTo: this.route });
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.addTypeForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.addTypeForm.patchValue({ image: Reader.result });
    };
    Reader.readAsDataURL(file);
  }

  editSubType(subType): void {
    this.store.dispatch(new PreserveType(this.addTypeForm.value));
    this.router.navigate(['edit-subtype'], { relativeTo: this.route, state: { subType } });
  }

  deleteSubType(subtype): void {
    this.store.dispatch(new RemoveSubType(subtype.name));
  }
}
