import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormArray, FormControl } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddSubType, UploadSubTypePicture } from '../../actions/type.actions';

@Component({
  selector: 'app-add-subtype',
  templateUrl: './add-subtype.component.html',
  styleUrls: ['./add-subtype.component.scss']
})
export class AddSubtypeComponent implements OnInit {

  addSubTypeForm = this.fb.group(
    {
      name: [''],
      description: [''],
      imageUri: [''],
      catalogPrices: [null],
      tpprice: [''],
      mpprice: [''],
      mcpprice: [''],
    }
  );

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) { }

  ngOnInit(): void {
  }

  save(): void {
    if (!this.addSubTypeForm.valid) {
      return console.log('FORM NOT VALID');
    }
    this.addSubTypeForm.patchValue({
      catalogPrices: [
        {
          pricePerUnit: this.addSubTypeForm.value.tpprice,
          priceTypeId: 1
        },
        {
          pricePerUnit: this.addSubTypeForm.value.mpprice,
          priceTypeId: 2
        },
        {
          pricePerUnit: this.addSubTypeForm.value.mcpprice,
          priceTypeId: 3
        }
      ]
    });
    if (this.addSubTypeForm.value.imageUri === '') {
      return alert('Select Image');
    }
    // validate uniq
    this.store.dispatch(new AddSubType(this.addSubTypeForm.value));
    this.router.navigate(['dashboard', 'manage-type', 'add-type']);
  }

  cancel(): void {
    this.router.navigate(['dashboard', 'manage-type', 'add-type']);
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.addSubTypeForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadSubTypePicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.addSubTypeForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }
}
