import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AddTypeComponent } from './add-type/add-type.component';
import { EditTypeComponent } from './edit-type/edit-type.component';
import { AddSubtypeComponent } from './add-type/add-subtype/add-subtype.component';
import { EditSubtypeComponent } from './add-type/edit-subtype/edit-subtype.component';
import { EditTypeAddSubtypeComponent } from './edit-type/add-subtype/add-subtype.component';
import { EditTypeEditSubtypeComponent } from './edit-type/edit-subtype/edit-subtype.component';

const routes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'add-type', children: [
    { path: '', component: AddTypeComponent},
    { path: 'add-subtype', component: AddSubtypeComponent},
    { path: 'edit-subtype', component: EditSubtypeComponent},
  ]},
  { path: 'edit-type', children: [
    { path: '', component: EditTypeComponent},
    { path: 'add-subtype', component: EditTypeAddSubtypeComponent},
    { path: 'edit-subtype', component: EditTypeEditSubtypeComponent},
  ]},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ManageTypeRoutingModule { }
