import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ManageTypeRoutingModule } from './manage-type-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/angular-material/angular-material.module';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { TypeState } from './state/type.state';

// COMPONENTS
import { HomeComponent } from './home/home.component';
import { AddTypeComponent } from './add-type/add-type.component';
import { AddSubtypeComponent } from './add-type/add-subtype/add-subtype.component';
import { EditTypeComponent } from './edit-type/edit-type.component';
import { EditSubtypeComponent } from './add-type/edit-subtype/edit-subtype.component';
import { EditTypeAddSubtypeComponent } from './edit-type/add-subtype/add-subtype.component';
import { EditTypeEditSubtypeComponent } from './edit-type/edit-subtype/edit-subtype.component';


@NgModule({
  declarations: [
    HomeComponent,
    AddTypeComponent,
    AddSubtypeComponent,
    EditTypeComponent,
    EditSubtypeComponent,
    EditTypeAddSubtypeComponent,
    EditTypeEditSubtypeComponent
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    ManageTypeRoutingModule,
    NgxsModule.forFeature([
      TypeState
    ])
  ]
})
export class ManageTypeModule { }
