import { CatalogSubType } from '../../../models/catalogSubType';
import { CatalogType } from 'src/app/models/catalogType';


export class FetchType {
  static readonly type = '[TYPE] fetchType';
}

export class CreateType {
  static readonly type = '[TYPE] createType';

  constructor(public payload: CatalogType) { }
}

export class UpdateType {
  static readonly type = '[TYPE] updateType';

  constructor(public payload: CatalogType) { }
}

export class PreserveType {
  static readonly type = '[TYPE] preserveType';

  constructor(public payload: CatalogType) { }
}

export class CleanType {
  static readonly type = '[TYPE] cleanType';
}

export class UploadTypePicture {
  static readonly type = '[TYPE] uploadTypePicture';

  constructor(public payload: any) { }
}

export class RemoveType {
  static readonly type = '[TYPE] removeType';

  constructor(public payload: number) { }
}

export class AddSubType {
  static readonly type = '[SUBTYPE] addSubType';

  constructor(public payload: CatalogSubType) { }
}

export class AddSubTypeToEditedType {
  static readonly type = '[SUBTYPE] addSubTypeToEditedType';

  constructor(public payload: CatalogSubType) { }
}

export class EditSubTypeToEditedType {
  static readonly type = '[SUBTYPE] editSubTypeToEditedType';

  constructor(public payload: CatalogSubType) { }
}

export class UploadSubTypePicture {
  static readonly type = '[TYPE] uploadSubTypePicture';

  constructor(public payload: any) { }
}

export class RemoveSubType {
  static readonly type = '[SUBTYPE] removeSubType';

  constructor(public payload: string) { }
}

export class DeleteSubType {
  static readonly type = '[SUBTYPE] deleteSubType';

  constructor(public payload: number) { }
}

export class CleanSubType {
  static readonly type = '[SUBTYPE] cleanSubType';

  constructor() { }
}
