import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormArray, FormControl } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Select } from '@ngxs/store';
import { TypeState } from '../../state/type.state';
import { Observable } from 'rxjs';
import { AddSubType, UploadSubTypePicture, RemoveSubType, CleanSubType, EditSubTypeToEditedType } from '../../actions/type.actions';

// Models
import { CatalogType } from 'src/app/models/catalogType';

@Component({
  selector: 'app-edit-subtype',
  templateUrl: './edit-subtype.component.html',
  styleUrls: ['./edit-subtype.component.scss']
})
export class EditTypeEditSubtypeComponent implements OnInit, OnDestroy {

  @Select(TypeState.getPreservedType) preservedType$: Observable<CatalogType>;
  type: CatalogType;

  previousName: string;
  editSubTypeForm = this.fb.group(
    {
      id: [null],
      catalogTypeId: [null],
      name: [''],
      description: [''],
      imageUri: [''],
      catalogPrices: [null],
      tpprice: [''],
      mpprice: [''],
      mcpprice: [''],
    }
  );

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      this.editSubTypeForm.patchValue(this.router.getCurrentNavigation().extras.state.subType);
      this.previousName = this.router.getCurrentNavigation().extras.state.subType.name;
    } else {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.preservedType$.subscribe().unsubscribe();
  }

  save(): void {
    if (!this.editSubTypeForm.valid) {
      return console.log('FORM NOT VALID');
    }
    // this.editSubTypeForm.patchValue({
    //   catalogPrices: [
    //     {
    //       pricePerUnit: this.editSubTypeForm.value.tpprice,
    //       priceTypeId: 1
    //     },
    //     {
    //       pricePerUnit: this.editSubTypeForm.value.mpprice,
    //       priceTypeId: 2
    //     },
    //     {
    //       pricePerUnit: this.editSubTypeForm.value.mcpprice,
    //       priceTypeId: 3
    //     }
    //   ]
    // });
    if (this.editSubTypeForm.value.imageUri === '') {
      return alert('Select Image');
    }
    // validate uniq
    // this.store.dispatch(new RemoveSubType(this.previousName));
    this.store.dispatch(new EditSubTypeToEditedType(this.editSubTypeForm.value));
    this.store.dispatch(new CleanSubType());
    this.preservedType$.subscribe(val => {
      return this.type = val;
    });
    this.type.catalogSubTypes.forEach(subtype => {
      if (subtype.name === this.previousName) {
        subtype.name = this.editSubTypeForm.value.name;
        subtype.description = this.editSubTypeForm.value.description;
        subtype.imageUri = this.editSubTypeForm.value.imageUri;
      }
    });
    this.router.navigate(['dashboard', 'manage-type', 'edit-type'], { state: { type: this.type } });
  }

  cancel(): void {
    this.store.dispatch(new CleanSubType());
    this.preservedType$.subscribe(val => {
      return this.type = val;
    });
    this.router.navigate(['dashboard', 'manage-type', 'edit-type'], { state: { type: this.type } });
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.editSubTypeForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadSubTypePicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.editSubTypeForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }
}
