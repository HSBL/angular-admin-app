import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTypeEditSubtypeComponent } from './edit-subtype.component';

describe('EditSubtypeComponent', () => {
  let component: EditTypeEditSubtypeComponent;
  let fixture: ComponentFixture<EditTypeEditSubtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTypeEditSubtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTypeEditSubtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
