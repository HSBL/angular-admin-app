import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { Observable } from 'rxjs';

import { Store } from '@ngxs/store';
import { Select } from '@ngxs/store';
import { TypeState } from '../state/type.state';
import {
  RemoveSubType,
  CleanSubType,
  UploadTypePicture,
  UpdateType,
  PreserveType,
  CleanType,
  AddSubType,
  DeleteSubType
} from '../actions/type.actions';

import { CatalogSubType } from 'src/app/models/catalogSubType';
import { CatalogType } from 'src/app/models/catalogType';

@Component({
  selector: 'app-edit-type',
  templateUrl: './edit-type.component.html',
  styleUrls: ['./edit-type.component.scss']
})
export class EditTypeComponent implements OnInit {

  typeId: number;
  @Select(TypeState.getLocalSubTypes) subTypes$: Observable<CatalogSubType[]>;
  @Select(TypeState.getPreservedType) preservedType$: Observable<CatalogType>;

  editTypeForm = this.fb.group(
    {
      id: [null],
      name: [''],
      description: [''],
      imageUri: [''],
      image: [null],
      catalogSubTypes: [null]
    }
  );

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private store: Store
  ) {
    if (!this.router.getCurrentNavigation().extras.state) {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
    this.typeId = this.router.getCurrentNavigation().extras.state.type.id;
    this.store.dispatch(new PreserveType(this.router.getCurrentNavigation().extras.state.type));
    this.router.getCurrentNavigation().extras.state.type.catalogSubTypes.forEach(element => {
      this.store.dispatch(new AddSubType(element));
    });
    this.preservedType$.subscribe(val => {

      return this.editTypeForm.patchValue({
        id: val.id,
        name: val.name,
        description: val.description,
        imageUri: val.imageUri
      });
    });
  }

  ngOnInit(): void {
  }

  save(): void {
    if (!this.editTypeForm.valid) {
      return console.log('FORM NOT VALID');
    }
    let subTypesLength: number;
    this.subTypes$.subscribe(val => {
      return subTypesLength = val.length;
    });
    if (subTypesLength < 1) {
      return alert('must input subtype');
    }
    if (this.editTypeForm.value.imageUri === '') {
      return alert('Select Image');
    }
    this.store.dispatch(new UpdateType(this.editTypeForm.value));
    this.store.dispatch(new CleanSubType());
    this.store.dispatch(new CleanType());
    this.router.navigate(['dashboard', 'manage-type']);
  }

  cancel(): void {
    this.store.dispatch(new CleanType());
    this.store.dispatch(new CleanSubType());
    this.router.navigate(['dashboard', 'manage-type']);
  }

  addSubtype(): void {
    this.subTypes$.subscribe(val => {
      return this.editTypeForm.patchValue({ catalogSubTypes: val });
    });
    this.store.dispatch(new PreserveType(this.editTypeForm.value));
    this.router.navigate(['add-subtype'], {
      relativeTo: this.route, state: { typeId: this.typeId }
    });
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.editTypeForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadTypePicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.editTypeForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }

  editSubType(subType): void {
    this.subTypes$.subscribe(val => {
      return this.editTypeForm.patchValue({ catalogSubTypes: val });
    });
    this.store.dispatch(new PreserveType(this.editTypeForm.value));
    this.router.navigate(['edit-subtype'], {
      relativeTo: this.route, state: { typeId: this.typeId, subType }
    });
  }

  deleteSubType(subtype): void {
    this.store.dispatch(new RemoveSubType(subtype.name)); // Delete Local State
    this.store.dispatch(new DeleteSubType(subtype.id)); // Delete Database
  }
}
