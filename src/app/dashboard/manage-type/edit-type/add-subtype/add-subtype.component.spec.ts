import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTypeAddSubtypeComponent } from './add-subtype.component';

describe('AddSubtypeComponent', () => {
  let component: EditTypeAddSubtypeComponent;
  let fixture: ComponentFixture<EditTypeAddSubtypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTypeAddSubtypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTypeAddSubtypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
