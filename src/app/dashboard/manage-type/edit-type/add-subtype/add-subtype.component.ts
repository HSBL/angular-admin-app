import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormArray, FormControl } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Select } from '@ngxs/store';
import { TypeState } from '../../state/type.state';
import { Observable } from 'rxjs';
import { AddSubType, UploadSubTypePicture, AddSubTypeToEditedType, CleanSubType } from '../../actions/type.actions';
import { CatalogType } from 'src/app/models/catalogType';

@Component({
  selector: 'app-add-subtype',
  templateUrl: './add-subtype.component.html',
  styleUrls: ['./add-subtype.component.scss']
})
export class EditTypeAddSubtypeComponent implements OnInit, OnDestroy {

  @Select(TypeState.getPreservedType) preservedType$: Observable<CatalogType>;
  type: CatalogType;

  addSubTypeForm = this.fb.group(
    {
      name: [''],
      description: [''],
      imageUri: [''],
      catalogPrices: [null],
      catalogTypeId: [null],
      tpprice: [''],
      mpprice: [''],
      mcpprice: [''],
    }
  );

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private store: Store
  ) {
    if (!this.router.getCurrentNavigation().extras.state) {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
    this.addSubTypeForm.patchValue({ catalogTypeId: this.router.getCurrentNavigation().extras.state.typeId });
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.preservedType$.subscribe().unsubscribe();
  }

  save(): void {
    if (!this.addSubTypeForm.valid) {
      return console.log('FORM NOT VALID');
    }
    this.addSubTypeForm.patchValue({
      catalogPrices: [
        {
          pricePerUnit: this.addSubTypeForm.value.tpprice,
          priceTypeId: 1
        },
        {
          pricePerUnit: this.addSubTypeForm.value.mpprice,
          priceTypeId: 2
        },
        {
          pricePerUnit: this.addSubTypeForm.value.mcpprice,
          priceTypeId: 3
        }
      ]
    });
    if (this.addSubTypeForm.value.imageUri === '') {
      return alert('Select Image');
    }
    // validate uniq
    this.store.dispatch(new AddSubTypeToEditedType(this.addSubTypeForm.value));

    // back to edit
    this.store.dispatch(new CleanSubType());
    this.preservedType$.subscribe(val => {
      return this.type = val;
    });
    this.router.navigate(['dashboard', 'manage-type', 'edit-type'], { state: { type: this.type } });
  }

  cancel(): void {
    this.store.dispatch(new CleanSubType());
    this.preservedType$.subscribe(val => {
      return this.type = val;
    });
    this.router.navigate(['dashboard', 'manage-type', 'edit-type'], { state: { type: this.type } });
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.addSubTypeForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      // this.store.dispatch(new UploadSubTypePicture(
      //   {
      //     image: Reader.result.toString().split(',')[1],
      //     name: this.addSubTypeForm.value.imageUri
      //   }));
    };
    Reader.readAsDataURL(file);
  }
}
