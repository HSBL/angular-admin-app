import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient } from '@angular/common/http';
import { CatalogType } from 'src/app/models/catalogType';

@Injectable({
  providedIn: 'root'
})
export class TypeService {

  constructor(private http: HttpClient) { }

  createType(payload: CatalogType) {
    return this.http.post<CatalogType>(environment.apiUrl + '/api/v1/CatalogType/save', payload);
  }


  fetchType() {
    return this.http.get<CatalogType[]>(environment.apiUrl + '/api/v1/CatalogType/all');
  }

  updateType(payload: CatalogType) {
    return this.http.put<CatalogType>(environment.apiUrl + `/api/v1/CatalogType/${payload.id}`, payload);
  }

  deleteType(id: number) {
    return this.http.delete(environment.apiUrl + `/api/v1/CatalogType/${id}`);
  }

  uploadPicture(payload: any) {
    return this.http.post<any>(environment.apiUrl + '/api/v1/CatalogType/UploadPicture', payload);
  }
}
