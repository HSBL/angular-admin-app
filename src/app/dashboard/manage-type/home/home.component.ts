import { Component, OnInit } from '@angular/core';
import { CatalogType } from 'src/app/models/catalogType';

import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { TypeState } from '../state/type.state';
import { FetchType, CleanSubType, CleanType } from '../actions/type.actions';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  @Select(TypeState.getTypes) types$: Observable<CatalogType[]>;

  constructor(
    private store: Store,
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.store.dispatch(new CleanSubType());
    this.store.dispatch(new CleanType());
    this.store.dispatch(new FetchType());
  }

  editType(type): void {
    this.router.navigate(['edit-type'], { relativeTo: this.route, state: { type } });
  }

}
