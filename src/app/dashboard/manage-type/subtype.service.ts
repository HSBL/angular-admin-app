import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';

import { HttpClient } from '@angular/common/http';
import { CatalogSubType } from 'src/app/models/catalogSubType';

@Injectable({
  providedIn: 'root'
})
export class SubtypeService {

  constructor(private http: HttpClient) { }

  addSubType(payload: CatalogSubType) {
    return this.http.post<CatalogSubType>(environment.apiUrl + `/api/v1/CatalogType/${payload.catalogTypeId}/AddSubType`, payload);
  }

  UpdateSubType(payload: CatalogSubType) {
    return this.http.put<CatalogSubType>(environment.apiUrl +
      `/api/v1/CatalogType/${payload.catalogTypeId}/EditOption/${payload.id}`, payload);
  }

  deleteSubType(payload: number) {
    return this.http.delete<number>(environment.apiUrl + `/api/v1/CatalogSubType/${payload}`);
  }

  uploadPicture(payload: any) {
    return this.http.post<any>(environment.apiUrl + '/api/v1/CatalogSubType/UploadPicture', payload);
  }
}
