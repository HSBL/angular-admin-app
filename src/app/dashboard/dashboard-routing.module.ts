import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { MaterialListComponent } from './material-list/material-list.component';
import { DashboardComponent } from './dashboard.component';
import { AddMaterialComponent } from './add-material/add-material.component';
import { AddOptionComponent } from './add-option/add-option.component';
import { EditOptionComponent } from './edit-option/edit-option.component';
import { EditProvincePriceComponent } from './edit-province-price/edit-province-price.component';
import { EditGlobalPriceComponent } from './edit-global-price/edit-global-price.component';
import { ManagePartnerAccountsComponent } from './other-pages/manage-partner-accounts/manage-partner-accounts.component';
import { AddUserAccountComponent } from './other-pages/add-user-account/add-user-account.component';
import { ManageInternalAccountComponent } from './other-pages/manage-internal-account/manage-internal-account.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent, children: [
      { path: '', component: HomeComponent },
      { path: 'global-material', component: MaterialListComponent },
      { path: 'add-material', component: AddMaterialComponent },
      { path: 'edit-material', loadChildren: () => import('./edit-material/edit-material.module').then(m => m.EditMaterialModule)  },
      { path: 'manage-type', loadChildren: () => import('./manage-type/manage-type.module').then(m => m.ManageTypeModule)  },
      { path: 'add-option', component: AddOptionComponent },
      { path: 'edit-option', component: EditOptionComponent },
      { path: 'edit-province-price', component: EditProvincePriceComponent },
      { path: 'edit-global-price', component: EditGlobalPriceComponent },
      { path: 'manage-partner-accounts', component: ManagePartnerAccountsComponent }, // MOCK PAGE FOR STYLES
      { path: 'add-user-account', component: AddUserAccountComponent }, // MOCK PAGE FOR STYLES
      { path: 'manage-internal-account', component: ManageInternalAccountComponent }, // MOCK PAGE FOR STYLES
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
