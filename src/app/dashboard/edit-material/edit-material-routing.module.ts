import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditMaterialComponent } from './edit-material.component';
import { AddOptionComponent } from './add-option/add-option.component';
import { EditOptionComponent } from './edit-option/edit-option.component';

const routes: Routes = [
  { path: ':id', component: EditMaterialComponent},
  { path: ':id/add-option', component: AddOptionComponent},
  { path: ':id/edit-option', component: EditOptionComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditMaterialRoutingModule { }
