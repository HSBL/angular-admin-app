import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { CatalogOption } from '../../models/catalogOption';
import { CatalogItem } from '../../models/catalogItem';
import {
  RemoveOption,
  AddMaterial,
  DestroyOption,
  AddOption,
  RemoveMaterial,
  DetailMaterial,
  EditMaterial,
  OverrideCsr,
  DeleteOption,
  GetSubTypes,
  UploadMaterialPicture
} from '../actions/material.actions';

@Component({
  selector: 'app-edit-material',
  templateUrl: './edit-material.component.html',
  styleUrls: ['./edit-material.component.scss']
})
export class EditMaterialComponent implements OnInit {

  options$: Observable<CatalogOption[]>;
  material$: Observable<CatalogItem>;

  materialForm = this.fb.group({
    id: [null],
    name: [''],
    editorId: ['editor'],
    imageUri: ['image.jpg'],
    description: [''],
    catalogOptions: [null],
    csr: [null]
  });


  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private store: Store,
    private fb: FormBuilder
  ) {
    this.store.dispatch(new DetailMaterial(Number(this.route.snapshot.paramMap.get('id'))));

    this.options$ = this.store.select(response => response.state.options);
    this.material$ = this.store.select(response => response.state.material);
    this.material$.subscribe(value => {
      if (value) {
        this.materialForm.patchValue({ name: value.name, description: value.description, id: value.id, imageUri: value.imageUri });
      }
    });
  }

  ngOnInit(): void {
    this.store.dispatch(new GetSubTypes());
  }

  cancel(): void {
    // TODO: delete image stored on bucket
    this.store.dispatch(new DestroyOption());
    this.router.navigate(['dashboard', 'global-material']);
  }
  save(): void {
    if (!this.materialForm.valid) {
      return console.log('FORM NOT VALID');
    }
    let optionsLength: number;
    this.options$.subscribe(val => {
      return optionsLength = val.length;
    });
    if (optionsLength < 1) {
      return alert('must input option');
    }
    if (this.materialForm.value.csr !== null) {
      this.store.dispatch(new OverrideCsr(
        {
          id: this.materialForm.value.id,
          editorId: 'editor',
          csr: this.materialForm.value.csr
        }));
    }
    this.materialForm.patchValue({ catalogOptions: null });
    this.store.dispatch(new EditMaterial(this.materialForm.value));
    this.store.dispatch(new DestroyOption());
    this.router.navigate(['dashboard', 'global-material']);
  }
  addOption(): void {
    // console.log(this.route.snapshot.paramMap.get('id'));
    this.router.navigate(['add-option'], { relativeTo: this.route, state: { materialForm: this.materialForm.value } });
  }
  deleteOption(option): void {
    this.store.dispatch(new RemoveOption(option.name)); // Only Remove from local state, no need to refetch.
    this.store.dispatch(new DeleteOption({ optionId: option.id, itemId: Number(this.route.snapshot.paramMap.get('id')) }));
  }
  editOption(option: CatalogOption): void {
    // console.log(option);
    this.router.navigate(['edit-option'], { relativeTo: this.route, state: { option, materialForm: this.materialForm.value } });
  }
  // TODO: file input validation
  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.materialForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadMaterialPicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.materialForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }
  getTpPrice(option: CatalogOption): number {
    return option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'tpprice')[0].pricePerUnit
      + (option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'tpprice')[0].pricePerUnit * option.csr / 100);
  }
  getMpPrice(option: CatalogOption): number {
    return option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mpprice')[0].pricePerUnit
      + (option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mpprice')[0].pricePerUnit * option.csr / 100);
  }
  getMcpPrice(option: CatalogOption): number {
    return option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mcpprice')[0].pricePerUnit
      + (option.catalogSubType.catalogPrices.filter(type => type.priceType.name === 'mcpprice')[0].pricePerUnit * option.csr / 100);
  }
}
