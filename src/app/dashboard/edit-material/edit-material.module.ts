import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditMaterialRoutingModule } from './edit-material-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularMaterialModule } from 'src/app/angular-material/angular-material.module';

// NGXS
import { NgxsModule } from '@ngxs/store';
import { AppState } from '../../state/state';

import { EditMaterialComponent } from './edit-material.component';
import { AddOptionComponent } from './add-option/add-option.component';
import { EditOptionComponent } from './edit-option/edit-option.component';

@NgModule({
  declarations: [EditMaterialComponent, AddOptionComponent, EditOptionComponent],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    EditMaterialRoutingModule,
    NgxsModule.forFeature([
      AppState
    ])
  ]
})
export class EditMaterialModule { }
