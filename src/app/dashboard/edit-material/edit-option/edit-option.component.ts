import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators } from '@angular/forms';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { AddOption, RemoveOption, EditMaterialEditOption, UploadOptionPicture } from '../../actions/material.actions';

// models
import { CatalogSubType } from '../../../models/catalogSubType';
import { CatalogItem } from '../../../models/catalogItem';
import { CatalogOption } from 'src/app/models/catalogOption';

@Component({
  selector: 'app-edit-option',
  templateUrl: './edit-option.component.html',
  styleUrls: ['./edit-option.component.scss']
})
export class EditOptionComponent implements OnInit {

  cachedMaterial: CatalogItem;
  optionId: number;
  optionName: string;
  options: CatalogOption[];
  unique = true;
  subtypes$: Observable<CatalogSubType[]>;
  selectedSubtype: CatalogSubType;
  quantifiers: string[] = ['pcs', 'kilo', 'ons', 'unit', 'pack'];

  optionForm = this.fb.group({
    id: [null],
    name: [''],
    weightInGram: [null],
    csr: [null, Validators.min(0)],
    quantifier: [''],
    catalogSubType: [null],
    catalogSubTypeId: [null],
    imageUri: ['image.jpg'],
    description: [''],
    editor: ['editor']
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private fb: FormBuilder,
    private store: Store
  ) {
    this.subtypes$ = this.store.select(response => response.state.subtypes);
    if (this.router.getCurrentNavigation().extras.state) {
      this.optionForm.patchValue(this.router.getCurrentNavigation().extras.state.option);
      // this.selectedSubtype = this.router.getCurrentNavigation().extras.state.option.catalogSubType;
      this.subtypes$.subscribe(subtypes => {
        subtypes.forEach(x => {
          if (x.id === this.router.getCurrentNavigation().extras.state.option.catalogSubType.id) {
            this.selectedSubtype = x;
            this.optionForm.patchValue({ catalogSubType: x });
          }
        });
      });
      this.optionId = this.router.getCurrentNavigation().extras.state.option.id;
      this.optionName = this.router.getCurrentNavigation().extras.state.option.name;
      this.cachedMaterial = this.router.getCurrentNavigation().extras.state.materialForm;
      this.options = this.store.selectSnapshot(response => response.state.options);
    } else {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
  }

  ngOnInit(): void {
  }

  // methods
  cancel(): void {
    // TODO: delete image stored on bucket
    this.router.navigate([
      'dashboard',
      'edit-material',
      this.route.snapshot.paramMap.get('id')
    ], { state: { cachedMaterial: this.cachedMaterial } });
  }
  save(): void {
    if (!this.optionForm.valid) {
      return console.log('FORM NOT VALID');
    }
    // Validate other option stored at state
    this.unique = true;
    this.options.forEach(option => {
      if (option.name === this.optionForm.value.name  && this.optionForm.value.name !== this.optionName ) {
        this.unique = false;
      }
    });
    if (!this.unique) {
      return alert('OPTION NAME ALREADY USED');
    }
    this.store.dispatch(new EditMaterialEditOption({
      itemId: this.route.snapshot.paramMap.get('id'), option: {
        id: this.optionId,
        catalogOptionName: this.optionForm.value.name,
        description: this.optionForm.value.description,
        editor: this.optionForm.value.editor,
        quantifier: this.optionForm.value.quantifier,
        imageUri: this.optionForm.value.imageUri,
        weightInGram: this.optionForm.value.weightInGram,
        subTypeId: this.selectedSubtype.id,
        csr: this.optionForm.value.csr
      }
    }));
    this.router.navigate([
      'dashboard',
      'edit-material',
      this.route.snapshot.paramMap.get('id')
    ], { state: { cachedMaterial: this.cachedMaterial } });
  }
  getSubsidied(value): number {
    return value + value * this.optionForm.value.csr / 100;
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.optionForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.store.dispatch(new UploadOptionPicture(
        {
          image: Reader.result.toString().split(',')[1],
          name: this.optionForm.value.imageUri
        }));
    };
    Reader.readAsDataURL(file);
  }
}
