import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators, RequiredValidator } from '@angular/forms';

// NGXS
import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';
import { EditMaterialAddOption, UploadOptionPicture } from '../../actions/material.actions';

// models
import { CatalogSubType } from '../../../models/catalogSubType';
import { CatalogItem } from '../../../models/catalogItem';
import { CatalogOption } from 'src/app/models/catalogOption';

@Component({
  selector: 'app-add-option',
  templateUrl: './add-option.component.html',
  styleUrls: ['./add-option.component.scss']
})
export class AddOptionComponent implements OnInit {

  previousUrl: string;
  options: CatalogOption[];
  unique = true;
  cachedMaterial: CatalogItem;
  subtypes$: Observable<CatalogSubType[]>;
  selectedSubtype: CatalogSubType;
  quantifiers: string[] = ['pcs', 'kilo', 'ons', 'unit', 'pack'];

  optionForm = this.fb.group({
    name: [''],
    weightInGram: [null, Validators.min(1)],
    csr: [0, Validators.min(0)],
    quantifier: [''],
    // catalogSubType: [null],
    catalogSubTypeId: [null],
    imageUri: ['image.jpg'],
    description: [''],
    editor: ['editor'],
    image: ['']
  });

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private fb: FormBuilder,
    private store: Store
  ) {
    // ROUTER GUARD
    if (!this.router.getCurrentNavigation().extras.state) {
      this.router.navigate(['dashboard']);
    }

    // CACHED MATERIAL
    this.cachedMaterial = this.router.getCurrentNavigation().extras.state.materialForm;
    this.subtypes$ = this.store.select(response => response.state.subtypes);

    // STORED OPTIONS
    this.options = this.store.selectSnapshot(response => response.state.options);
  }

  ngOnInit(): void {
  }

  // methods
  cancel(): void {
    this.router.navigate([
      'dashboard',
      'edit-material',
      this.route.snapshot.paramMap.get('id')
    ], { state: { cachedMaterial: this.cachedMaterial } });
  }
  save(): void {
    if (!this.optionForm.valid) {
      return console.log('FORM NOT VALID');
    }
    // Validate other option stored at state
    this.unique = true;
    this.options.forEach(option => {
      if (option.name === this.optionForm.value.name) {
        this.unique = false;
      }
    });
    if (!this.unique) {
      return alert('OPTION NAME ALREADY USED');
    }
    if (this.optionForm.value.image === '') {
      return alert('Select Option Image');
    }
    this.store.dispatch(new UploadOptionPicture(
      {
        image: this.optionForm.value.image.split(',')[1],
        name: this.optionForm.value.imageUri
      }));
    this.store.dispatch(new EditMaterialAddOption(
      {
        itemId: this.route.snapshot.paramMap.get('id'),
        option:
        {
          catalogOptionName: this.optionForm.value.name,
          description: this.optionForm.value.description,
          editor: this.optionForm.value.editor,
          quantifier: this.optionForm.value.quantifier,
          imageUri: this.optionForm.value.imageUri,
          weightInGram: this.optionForm.value.weightInGram,
          subTypeId: this.selectedSubtype.id,
          csr: this.optionForm.value.csr
        }
      }));
    this.router.navigate([
      'dashboard',
      'edit-material',
      this.route.snapshot.paramMap.get('id')
    ]);
  }
  getSubsidied(value): number {
    return value + value * this.optionForm.value.csr / 100;
  }

  imageChange($event) {
    this.readBase64($event.target);
  }
  readBase64(inputValue: any): void {
    this.optionForm.patchValue({ imageUri: inputValue.files[0].name });
    const file: File = inputValue.files[0];
    const Reader: FileReader = new FileReader();

    Reader.onloadend = (e) => {
      this.optionForm.patchValue({ image: Reader.result });
    };
    Reader.readAsDataURL(file);
  }
}
