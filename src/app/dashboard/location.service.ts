import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

import { CatalogLocation } from '../models/catalogLocation';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  constructor(private http: HttpClient) { }

  fetcLocations() {
    return this.http.get<CatalogLocation[]>(environment.apiUrl + '/api/v1/CatalogLocation/all');
  }

}
