import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { MatDialog } from '@angular/material/dialog';

import { Store } from '@ngxs/store';
import { Observable } from 'rxjs';

import { CatalogSubType } from '../../models/catalogSubType';
import { CatalogPrice } from '../../models/catalogPrice';
import { EditGlobalDialogComponent } from './edit-global-dialog/edit-global-dialog.component';

import { StorePrice, DestroyPrice } from '../actions/material.actions';

@Component({
  selector: 'app-edit-global-price',
  templateUrl: './edit-global-price.component.html',
  styleUrls: ['./edit-global-price.component.scss']
})
export class EditGlobalPriceComponent implements OnInit {

  prices$: Observable<CatalogPrice[]>;
  // subtypes$: Observable<CatalogSubType[]>;
  selectedSubtype: CatalogSubType;

  constructor(
    private store: Store,
    private router: Router,
    private location: Location,
    public dialog: MatDialog
  ) {
    if (this.router.getCurrentNavigation().extras.state) {
      // Clear the store first
      this.store.dispatch(new DestroyPrice());
      this.selectedSubtype = this.router.getCurrentNavigation().extras.state.subtype;

      // Store Initial price to state management
      // this.store.dispatch(new FetchPrice({ subTypeId: this.selectedSubtype.id, locationId: this.catalogLocation.id }));
      this.router.getCurrentNavigation().extras.state.subtype.catalogPrices.forEach(price => {
        this.store.dispatch(new StorePrice(price));
      });
    } else {
      // Router Guard
      this.router.navigate(['dashboard']);
    }
    this.prices$ = this.store.select(response => response.state.prices);

    // this.subtypes$ = this.store.select(response => response.state.subtypes);
  }

  ngOnInit(): void {
  }

  // methods
  save(): void {
    this.router.navigate(['dashboard']);
  }

  editPriceDialog(price): void {
    price.subType = this.selectedSubtype;
    this.dialog.open(EditGlobalDialogComponent, { data: price });
  }
}
