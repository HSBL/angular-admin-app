import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGlobalPriceComponent } from './edit-global-price.component';

describe('EditGlobalPriceComponent', () => {
  let component: EditGlobalPriceComponent;
  let fixture: ComponentFixture<EditGlobalPriceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGlobalPriceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGlobalPriceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
