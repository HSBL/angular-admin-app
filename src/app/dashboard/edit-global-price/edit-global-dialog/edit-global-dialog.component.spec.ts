import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditGlobalDialogComponent } from './edit-global-dialog.component';

describe('EditGlobalDialogComponent', () => {
  let component: EditGlobalDialogComponent;
  let fixture: ComponentFixture<EditGlobalDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditGlobalDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditGlobalDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
