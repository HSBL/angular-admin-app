import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators } from '@angular/forms';

import { Store } from '@ngxs/store';
import { EditGlobalPrice, EditPrice, EditPriceOverride } from '../../actions/material.actions';
import { CatalogPrice } from 'src/app/models/catalogPrice';
import { CatalogSubType } from 'src/app/models/catalogSubType';

@Component({
  selector: 'app-edit-global-dialog',
  templateUrl: './edit-global-dialog.component.html',
  styleUrls: ['./edit-global-dialog.component.scss']
})
export class EditGlobalDialogComponent implements OnInit {

  overwrite = false;
  subType: CatalogSubType;
  newPriceForm = this.fb.group({
    newPrice: [null, Validators.min(0)],
    overwrite: [null]
  });

  constructor(
    public dialogRef: MatDialogRef<EditGlobalDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private store: Store
  ) {
    this.subType = this.data.subType;
    this.subType.catalogPrices[0].priceType.id = this.data.priceType.id;
  }

  ngOnInit(): void {
  }

  cancel(): void {
    this.dialogRef.close();
  }
  apply(): void {
    if (!this.newPriceForm.valid) {
      return console.log('FORM NOT VALID');
    }
    if (this.newPriceForm.value.overwrite) {
      this.store.dispatch(new EditPriceOverride(
        {
          id: this.subType.catalogPrices[0].id,
          pricePerUnit: this.newPriceForm.value.newPrice,
          catalogSubTypeId: this.subType.id,
          editorId: 'override'
        }));
    }
    this.subType.catalogPrices[0].pricePerUnit = this.newPriceForm.value.newPrice;
    this.store.dispatch(new EditGlobalPrice(this.subType));
    this.dialogRef.close();
  }
}
